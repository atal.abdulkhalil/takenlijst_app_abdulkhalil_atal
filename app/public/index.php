<?php

require __DIR__ . '/../vendor/autoload.php';
$router = new \Bramus\Router\Router();

$router->setNamespace('\Http');

$router->before('GET|POST', '/.*', function () {
    session_start();
});

$router->get('/', function () {
    header('Location: tasks');
    exit();
});



$router->get('/tasks', 'TaskController@overview');
$router->post('/tasks/create', 'TaskController@create');
$router->get('/tasks/(\d+)/edit', 'TaskController@showEdit');
$router->post('/tasks/(\d+)/edit', 'TaskController@edit');
$router->get('/tasks/(\d+)/delete', 'TaskController@showDelete');
$router->post('/tasks/(\d+)/delete', 'TaskController@delete');


$router->get('/login', 'AuthController@showLogin');
$router->post('/login', 'AuthController@login');
$router->post('/logout', 'AuthController@logout');

$router->run();