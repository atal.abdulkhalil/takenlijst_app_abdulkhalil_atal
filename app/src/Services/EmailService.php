<?php


namespace Services;
require_once (__DIR__ . '/../../config/mail.php');

class EmailService
{
    private \Swift_Mailer $mailer;


    /**
     * EmailService constructor.
     */
    public function __construct()
    {
        $transport = (new \Swift_SmtpTransport(MAIL_HOST, MAIL_PORT))
            ->setUsername(MAIL_USERNAME)
            ->setPassword(MAIL_PASSWORD);

        $this->mailer = new \Swift_Mailer($transport);
    }

    public function test(string $email, string $username, array $taskArray){
        // Create the message
        $message = (new \Swift_Message())
            // Add subject
            ->setSubject('Overzicht van mijn taken')

            //Put the From address
            ->setFrom(['takenlijst@odisee.be' => 'Takenlijstmanager'])

            // Include several To addresses
            ->setTo([$email => $username]);
        $plain = '';
        $html = '<ul>';
        foreach ($taskArray as $task) {
            $plain .= $task->getName() . "\n";
            $html .= '<li>' . htmlentities($task->getName()) . '</li>';
        }
        $html .= '</ul>';

        $message->setBody($html, 'text/html' );
        $message->addPart($plain, 'text/plain');
        $this->mailer->send($message);
    }

}