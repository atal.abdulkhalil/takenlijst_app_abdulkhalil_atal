<?php


namespace Http;


use Models\Task;
use Services\DatabaseConnector;

class AuthController
{
    protected \Doctrine\DBAL\Connection $db;
    protected \Twig\Environment $twig;

    /**
     * AuthController constructor.
     */
    public function __construct()
    {
        $loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../../resources/templates');
        $this->twig = new \Twig\Environment($loader);

        $this->db = DatabaseConnector::getDBConnection();
    }


    public function showLogin()
    {
        //if already logged in
        if (isset($_SESSION['user'])) {
            header('Location: /tasks');
            exit();
        }

        $formErrors = isset($_SESSION['flash']['formErrors']) ? $_SESSION['flash']['formErrors'] : [];
        $username = isset($_SESSION['flash']['username']) ? $_SESSION['flash']['username'] : '';
        unset($_SESSION['flash']);

        echo $this->twig->render('pages/login.twig', [
        'username' => $username,
        'formErrors' => $formErrors
        ]);
    }


    public function login()
    {
        $formErrors = [];

        $username = isset($_POST['username']) ? trim($_POST['username']) : '';
        $password = isset($_POST['password']) ? trim($_POST['password']) : '';

        if (isset($_POST['moduleAction']) && ($_POST['moduleAction'] == 'login')){
            $stmt = $this->db->prepare('SELECT * FROM users WHERE username = ?');
            $stmt->execute([$username]);
            $user = $stmt->fetchAssociative();

            if (($user !== false) && (password_verify($password, $user['password']))) {
                $_SESSION['user'] = $user;
                header('Location: /tasks');
                exit();
            }
            else {
                $formErrors[] = 'invalid login credentials';
                $_SESSION['flash'] = ['formErrors' => $formErrors,
                    'username' => $username];
            }
        }
        header('Location: login');
        exit();
    }


    public function logout()
    {
        $_SESSION = [];

        if (ini_get('session.use_cookies')) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
            $params['path'], $params['domain'],
            $params['secure'], $params['httponly']
            );
        }
        session_destroy();
        header('Location: login');
        exit();
    }
}